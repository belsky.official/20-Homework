// Fill out your copyright notice in the Description page of Project Settings.


#include "Floor.h"
#include "FoodOrBonus/Food.h"
#include "FoodOrBonus/FoodFastSpeed.h"
#include "FoodOrBonus/FoodSlowSpeed.h"
#include "FoodOrBonus/FoodSuperFastSpeed.h"
#include "FoodOrBonus/FoodSuperSlowSpeed.h"
#include "MagicNumberTypes.h"
#include "SnakeElementBase.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AFloor::AFloor()
{
	PrimaryActorTick.bCanEverTick = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>("RootComponent");
}

void AFloor::BeginPlay()
{
	Super::BeginPlay();

	GenerateFloor();
	GenerateCirclePortal();
	GenerateWall();	
}

void AFloor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

//����� ��������� ����
void AFloor::GenerateFloor()
{
	AllSegmentLocations.Empty();

	//�������� ������ �� X,Y,Z ��� Floor
	FVector L_Scale = FVector(1.01, 1.01, 1.01);

	//�������� ������� ���������� ��� Location
	FVector L_Location = FVector::ZeroVector;

	int L_MapCenter = (MapSize / 2 * ELEMENT_SIZE) + ELEMENT_SIZE;
	MapCenter = L_MapCenter;

	for (int i = 1; i <= MapSize; i++)
	{
		Y = i * ELEMENT_SIZE - L_MapCenter;

		for (int j = 1; j <= MapSize; j++)
		{
			X = j * ELEMENT_SIZE - L_MapCenter;

			L_Location = UKismetMathLibrary::MakeVector(X, Y, -ELEMENT_SIZE);

			FTransform SpawnTransform = UKismetMathLibrary::MakeTransform(L_Location, FRotator::ZeroRotator, L_Scale);


			TArray<UStaticMeshComponent*> FloorMeshes;
			Floor = GetWorld()->SpawnActor<AFloorElement>(FloorClass, SpawnTransform);
			FloorMeshes.Add(Floor->GetStaticMeshComponent());

			for (auto FloorElement : FloorMeshes)
			{
				int32 RandI = FMath::RandRange(0, 2);
				if (IsValid(ElementMaterials) && IsValid(FloorElement))
				{
					FloorElement->SetMaterial(0, ElementMaterials);
				}
			}

			AllSegmentLocations.Add(L_Location);
		}
	}
}

//����� ��������� ����� ������ �����
void AFloor::GenerateCirclePortal()
{
	FVector L_Scale = FVector(1, 1, 1);
	FVector L_Location;

	int L_MapSize = MapSize + 2;

	int L_MapCenter = (L_MapSize / 2 * ELEMENT_SIZE) /*+ ELEMENT_SIZE * 4*/;

	for (int i = -1; i < L_MapSize; i++)
	{
		Y = i * ELEMENT_SIZE - L_MapCenter;

		for (int j = -1; j < L_MapSize; j++)
		{
			X = j * ELEMENT_SIZE - L_MapCenter;

			L_Location = UKismetMathLibrary::MakeVector(X + ELEMENT_SIZE, Y + ELEMENT_SIZE, 0.f);

			if (i == -1 && j == 0 || i == -1 && j == 1 || i == -1 && j == 2 || i == -1 && j == 3 || i == -1 && j == 4 ||
				i == -1 && j == 5 || i == -1 && j == 6 || i == -1 && j == 7 || i == -1 && j == 8 || i == -1 && j == 9 ||
				i == -1 && j == 10 || i == -1 && j == 11 || i == -1 && j == 12 || i == -1 && j == 13 || i == -1 && j == 14 ||
				
				i == 15 && j == 0 || i == 15 && j == 1 || i == 15 && j == 2 || i == 15 && j == 3 || i == 15 && j == 4 ||
				i == 15 && j == 5 || i == 15 && j == 6 || i == 15 && j == 7 || i == 15 && j == 8 || i == 15 && j == 9 ||
				i == 15 && j == 10 || i == 15 && j == 11 || i == 15 && j == 12 || i == 15 && j == 13 || i == 15 && j == 14 ||
				
				i == 1 && j == -1 || i == 2 && j == -1 || i == 3 && j == -1 || i == 4 && j == -1 || i == 5 && j == -1 ||
				i == 6 && j == -1 || i == 7 && j == -1 || i == 8 && j == -1 || i == 9 && j == -1 || i == 10 && j == -1 ||
				i == 11 && j == -1 || i == 12 && j == -1 || i == 13 && j == -1 || i == 14 && j == -1 || i == 0 && j == -1 ||
				
				i == 1 && j == 15 || i == 2 && j == 15 || i == 3 && j == 15 || i == 4 && j == 15 || i == 5 && j == 15 ||
				i == 6 && j == 15 || i == 7 && j == 15 || i == 8 && j == 15 || i == 9 && j == 15 || i == 10 && j == 15 ||
				i == 11 && j == 15 || i == 12 && j == 15 || i == 13 && j == 15 || i == 14 && j == 15 || i == 0 && j == 15 ||

				i == -1 && j == -1 || i == 15 && j == 15 || i == -1 && j == 15 || i == 15 && j == -1)
			{
				Portal = GetWorld()->SpawnActor<APortal>(PortalClass, L_Location, FRotator::ZeroRotator);
			}
		}
	}
}

//����� ��������� ����������
void AFloor::GenerateWall()
{
	FVector L_Scale = FVector(1.01, 1.01, 1.01);
	FVector L_Location;

	int L_MapSize = MapSize + 2;

	int L_MapCenter = (L_MapSize / 2 * ELEMENT_SIZE) + ELEMENT_SIZE;

	for (int i = 0; i < L_MapSize; i++)
	{
		Y = i * ELEMENT_SIZE - L_MapCenter;

		for (int j = 0; j < L_MapSize; j++)
		{
			X = j * ELEMENT_SIZE - L_MapCenter;

			L_Location = UKismetMathLibrary::MakeVector(X + ELEMENT_SIZE, Y + ELEMENT_SIZE, 0.f);

			if (i == 8 && j == 8 || i == 8 && j == 7 || i == 8 && j == 9 || i == 7 && j == 8 || i == 9 && j == 8 ||
				i == 7 && j == 9 || i == 9 && j == 9 || i == 7 && j == 7 || i == 9 && j == 7 || i == 6 && j == 8 ||
				i == 10 && j == 8 || i == 8 && j == 10 || i == 8 && j == 6)
			{
				WallZone = GetWorld()->SpawnActor<AWallKillZone>(WallClass, L_Location, FRotator::ZeroRotator);
				AllLocationsWall.Add(L_Location);
			}
		}
	}
}

//����� ����������� ��������� ��� ��� ������ ��� ������ � ����������
FVector AFloor::GetRandomFoodPos()
{
	FVector FoodLocations;
	FVector SnakeLocation(505, 0, 0);
	bool bIsDifferent = false;
	do
	{
		FoodLocations = AllSegmentLocations[FMath::RandRange(0, AllSegmentLocations.Num() - 1)];
		FoodLocations.Z = 0.f;
		for (int i = 0; i < AllLocationsWall.Num(); i++)
		{ 
			if ((FoodLocations.X != AllLocationsWall[i].X) && (FoodLocations.Y != AllLocationsWall[i].Y)  && (FoodLocations != SnakeLocation))
			{ 
				bIsDifferent = true;
			}
			else
			{
				continue;
			}
		}

	} while (bIsDifferent == false);
	return FoodLocations;
}


// ������ ������ ��� � �������
void AFloor::AddFood()
{
	FoodRef = GetWorld()->SpawnActor<AFood>(FoodClass, GetRandomFoodPos(), FRotator::ZeroRotator);
}
void AFloor::AddBonus()
{
	int32 RandomBonus = FMath::RandRange(0, 1);
	switch (RandomBonus)
	{
	case 0:
		FoodBonusFastSpeed = GetWorld()->SpawnActor<AFoodFastSpeed>(FirstBonus, GetRandomFoodPos(), FRotator::ZeroRotator);
		break;
	case 1:
		FoodBonusSlowSpeed = GetWorld()->SpawnActor<AFoodSlowSpeed>(SecondBonus, GetRandomFoodPos(), FRotator::ZeroRotator);
		break;
	}
}
void AFloor::AddSuperBonus()
{
	int32 RandomBonus = FMath::RandRange(0, 1);
	switch (RandomBonus)
	{
	case 0:
		FoodBonusSuperFastSpeed = GetWorld()->SpawnActor<AFoodSuperFastSpeed>(ThirdBonus, GetRandomFoodPos(), FRotator::ZeroRotator);
		break;
	case 1:
		FoodBonusSuperSlowSpeed = GetWorld()->SpawnActor<AFoodSuperSlowSpeed>(FourthBonus, GetRandomFoodPos(), FRotator::ZeroRotator);
		break;
	}
}


