// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeGameInstance.h"

void USnakeGameInstance::Init()
{
	Super::Init();
}

void USnakeGameInstance::SetMapSize(int NewMapSize)
{
	this->MapSize = NewMapSize;
}

int USnakeGameInstance::GetMapSize() const
{
	return MapSize;
}
