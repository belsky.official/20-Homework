// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WallAndPortal/WallKillZone.h"
#include "WallAndPortal/Portal.h"
#include "FloorElement.h"
#include "GameFramework/Actor.h"
#include "Floor.generated.h"

class AFood;
class UMaterialInterface;

class AFoodFastSpeed;
class AFoodSlowSpeed;

class AFoodSuperFastSpeed;
class AFoodSuperSlowSpeed;


UCLASS()
class THEFINALSNAKE_API AFloor : public AActor
{
	GENERATED_BODY()
	
public:	
	AFloor();

	UPROPERTY()
	APortal* Portal;

	UPROPERTY()
	AWallKillZone* WallZone;

	UPROPERTY()
	AFloorElement* Floor;

	UPROPERTY()
	AFood* FoodRef;

	UPROPERTY()
	AFoodFastSpeed* FoodBonusFastSpeed;

	UPROPERTY()
	AFoodSlowSpeed* FoodBonusSlowSpeed;

	UPROPERTY()
	AFoodSuperFastSpeed* FoodBonusSuperFastSpeed;

	UPROPERTY()
	AFoodSuperSlowSpeed* FoodBonusSuperSlowSpeed;

protected:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	//����� ������ ������ ����
	void GenerateFloor();

	//����� ������ ����� �� ���������
	void GenerateCirclePortal();

	//����� ������ ���������� � �����
	void GenerateWall();

	UFUNCTION()
	FVector GetRandomFoodPos();

	TArray<FVector> AllSegmentLocations;

	TArray<FVector> AllLocationsWall;

	UPROPERTY()
	int MapSize = 15;

	UPROPERTY(EditDefaultsOnly, Category = "Map Settings | Visual")
	UMaterialInterface* ElementMaterials;

	UPROPERTY(EditDefaultsOnly, Category = "Food AND Bonus")
	TSubclassOf<AFood> FoodClass;

	UPROPERTY(EditDefaultsOnly, Category = "Food AND Bonus")
	TSubclassOf<AFoodFastSpeed> FirstBonus;

	UPROPERTY(EditDefaultsOnly, Category = "Food AND Bonus")
	TSubclassOf<AFoodSlowSpeed> SecondBonus;

	UPROPERTY(EditDefaultsOnly, Category = "Food AND Bonus")
	TSubclassOf<AFoodSuperFastSpeed> ThirdBonus;

	UPROPERTY(EditDefaultsOnly, Category = "Food AND Bonus")
	TSubclassOf<AFoodSuperSlowSpeed> FourthBonus;

	UPROPERTY(EditDefaultsOnly, Category = "Wall Components|PortalBP")
	TSubclassOf<APortal> PortalClass;

	UPROPERTY(EditDefaultsOnly, Category = "Wall Components|WallBP")
	TSubclassOf<AWallKillZone> WallClass;

	UPROPERTY(EditDefaultsOnly, Category = "Wall Components|FloorBP")
	TSubclassOf<AFloorElement> FloorClass;

	float X;

	float Y;

	float MapCenter;

public:	

	void AddFood();
	void AddBonus();
	void AddSuperBonus();

};
