// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FloorElement.generated.h"

UCLASS()
class THEFINALSNAKE_API AFloorElement : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFloorElement();

protected:

	UPROPERTY(EditDefaultsOnly, Category="Component")
	UStaticMeshComponent* StaticMeshComponent;

public:	
	
	FORCEINLINE UStaticMeshComponent* GetStaticMeshComponent() { return StaticMeshComponent; }
};
 