// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlayerPawnBase.h"
#include "Sound/SoundCue.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

//������� ����������� ��� ����������� ������ � cpp �����
class ASnakeElementBase;

//����������� ENUM ��� ��������
UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class THEFINALSNAKE_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	//� �������� ������ �� ���� BP ��������� ������ �������� ������
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	//� �������� ������ �� ���� BP ��������� ������� �������� ������
	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	//� �������� ������ �� ���� BP ��������� �������� ������
	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	//������� ��������� ��� ��������� ������ � ������ ���
	UPROPERTY(BlueprintReadOnly)
	bool bIsFirstSpawn;

	//���������� ����� �� ������ ������
	UPROPERTY(BlueprintReadOnly)
	float ScoreRightNow;

	//���������� ����� ��� ����������� ������
	UPROPERTY(EditDefaultsOnly)
	float ScoreMax;

	UPROPERTY(EditDefaultsOnly)
	int HealthMax = 5;

	UPROPERTY(BlueprintReadOnly)
	int Health = 3; 

	UPROPERTY(BlueprintReadOnly)
	bool bIsGameStart = false;

	//� �������� � BP �������� ������� �� ��������� ������
	UPROPERTY()
	TArray <ASnakeElementBase*> SnakeElements;

	//� �������� � BP ���������� � ��������� �������� ������
	UPROPERTY()
	EMovementDirection LastMoveDirection;

	//������ � ���� ��� ������ ��������� ���� ������ �� ����� ���� � ����� ������
	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* ElementMaterialFirst;

	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* ElementMaterialSecond;

	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* DeadMaterial;

	UPROPERTY()
	APlayerPawnBase* PlayerPawnBase;

	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	USoundCue* MoveSound;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Destroyed() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//������� ���������� ��������� ������ � ������ ��������� ������
	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum = 1);

	//������� ������ �������� ������
	UFUNCTION(BlueprintCallable)
	void Move();

	//����� ���������� �� ����������� � ������ Actor
	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
};