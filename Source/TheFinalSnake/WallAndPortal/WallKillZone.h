// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TheFinalSnake/Interactablee.h"
#include "WallKillZone.generated.h"

UCLASS()
class THEFINALSNAKE_API AWallKillZone : public AActor, public IInteractablee
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWallKillZone();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//����� �������������� � ������ �������
	virtual void Interact(AActor* Interactor, bool bIsHead) override;

};
