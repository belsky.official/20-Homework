// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TheFinalSnake/Interactablee.h"
#include "TheFinalSnake/MagicNumberTypes.h"
#include "Sound/SoundCue.h"
#include "GameFramework/Actor.h"
#include "Portal.generated.h"

class UHierarchicalInstancedStaticMeshComponent;

UCLASS()
class THEFINALSNAKE_API APortal : public AActor, public IInteractablee
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APortal();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interactor, bool bIsHead) override;
protected:

	UPROPERTY(EditDefaultsOnly, Category = "Component")
	UStaticMeshComponent* StaticMeshComp;

	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	USoundCue* PortalSound;
};
