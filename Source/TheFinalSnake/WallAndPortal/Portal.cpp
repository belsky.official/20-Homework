// Fill out your copyright notice in the Description page of Project Settings.


#include "Portal.h"
#include "TheFinalSnake/SnakeBase.h"
#include "TheFinalSnake/SnakeElementBase.h"
#include "Kismet/GameplayStatics.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"

// Sets default values
APortal::APortal()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>("MeshComp");
	RootComponent = StaticMeshComp;
	float Scale = 0.95f;
	StaticMeshComp->SetRelativeScale3D(FVector(Scale, Scale, Scale));
}

// Called when the game starts or when spawned
void APortal::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APortal::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APortal::Interact(AActor* Interactor, bool bIsHead)
{
	IInteractablee::Interact(Interactor, bIsHead);
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{

		if ((Snake->Health - 1) >= 1)
		{
			Snake->Health--;

			FVector NewLocationForHead(505, 0, 0);
			Snake->SnakeElements[0]->SetActorLocation(NewLocationForHead);
			Snake->LastMoveDirection = EMovementDirection::DOWN;

			for (int i = 1; i < Snake->SnakeElements.Num(); i++)
			{
				FVector NewLocation;
				NewLocation = Snake->SnakeElements[i - 1]->GetActorLocation();
				NewLocation.X += Snake->ElementSize;

				Snake->SnakeElements[0]->ToggleCollision();

				Snake->SnakeElements[i]->SetActorLocation(NewLocation);

				Snake->SnakeElements[0]->ToggleCollision();
			}
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), PortalSound, FVector(0, 0, 0));
		}
		else
		{
			Snake->Destroy();
			Snake->Health--;
		}
	}
}

