// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactablee.h"
#include "SnakeElementBase.generated.h"

//������� ����������� ��� ����������� ������ � cpp �����
class UStaticMeshComponent;
class ASnakeBase;

UCLASS()
class THEFINALSNAKE_API ASnakeElementBase : public AActor, public IInteractablee
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeElementBase();

	//���������� MeshComponent � BP
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	//�������� Actor SnakeBase
	UPROPERTY()
	ASnakeBase* SnakeOwner;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//��������������� ������ ��� ������ � ��� � BP ��� ������ ������
	UFUNCTION(BlueprintNativeEvent)
	void SetFirstElementType();
	void SetFirstElementType_Implementation();

	//����� ����������
	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	//����� ���������� �� ����������� ���������� 
	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
							AActor* OtherActor, UPrimitiveComponent* OtherComp,
							int32 OtherBodyIndex,
							bool bFromSweep, 
							const FHitResult& SweepResult);

	//����� ������������ ��������
	UFUNCTION()
	void ToggleCollision();
};
