// Fill out your copyright notice in the Description page of Project Settings.
#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Floor.h"
#include "FoodOrBonus/Food.h"
#include "FoodOrBonus/FoodFastSpeed.h"
#include "FoodOrBonus/FoodSlowSpeed.h"
#include "FoodOrBonus/FoodSuperFastSpeed.h"
#include "FoodOrBonus/FoodSuperSlowSpeed.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//��������� Pawn ������ "PawnCamera"
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
		
	//��������������� �������� �� ������� ��� ������� ������
	TimerForLevelCurrent = TimerForLevelStart;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();

	//������� ������ �� ������� ��������
	SetActorRotation(FRotator(-90, 0, 0));
	SetActorLocation(FVector(0, 0, 1650));

	//����� Actor ������
	CreateFloorActor();
	CreateSnakeActor();

}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//��������� ������� ������
	if (!DecreaseTimerHandle.IsValid() && bIsGameStartExitMenu)
	{
		GetWorld()->GetTimerManager().SetTimer(DecreaseTimerHandle, this, &APlayerPawnBase::DecreaseTimeLevel, 1.0f, true);

	}

	//���� ������ ������ - ���������� ������� ������
	if (!IsValid(SnakeActor))
	{
		GetWorld()->GetTimerManager().ClearTimer(DecreaseTimerHandle);
		bIsOpenWidgetMenuRestartExit = true;
		DestroyAllAfterEnd();
	}
	if (TimerForLevelCurrent == 0)
	{
		GetWorld()->GetTimerManager().ClearTimer(DecreaseTimerHandle);
		bIsOpenWidgetMenuRestartExit = true;
		DestroyAllAfterEnd();
		SnakeActor->Destroy();
	}

	//���� ������ ������� - ����� ����� �������
	if (DecreaseTimerHandle.IsValid() && bIsGameStartExitMenu)
	{
		if (!IsValid(FloorActor->FoodRef))
		{
			FloorActor->AddFood();
			BufferTimerForBonus++;
			BufferTimerForSuperBonus++;
		}
		if ((!IsValid(FloorActor->FoodBonusFastSpeed)) && 
			(!IsValid(FloorActor->FoodBonusSlowSpeed)) &&
			(BufferTimerForBonus >= HowOftenSpawnBonus))
		{
			FloorActor->AddBonus();
			BufferTimerForBonus = 0;
		}
		if ((!IsValid(FloorActor->FoodBonusSuperFastSpeed)) &&
			(!IsValid(FloorActor->FoodBonusSuperSlowSpeed)) &&
			(BufferTimerForSuperBonus >= HowOftenSpawnSuperBonus))
		{
			FloorActor->AddSuperBonus();
			BufferTimerForSuperBonus = 0;
		}
		
	}
}


// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//��� ������� Space ����������� �������� ������
	PlayerInputComponent->BindAxis("StartGame", this, &APlayerPawnBase::HandlePlayerStartGameInput);

	//��� ������������ �������� ����� Pawn ���������� ������ ��������
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}


//����� ������ ������ �� ������ �� Defaults ���� BP ������ 
void APlayerPawnBase::CreateSnakeActor()
{
	FRotator SnakeStartLocation(505, 0, 0);
	FTransform NewTransform(SnakeStartLocation);
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

//����� ������ ���� ��� ������
void APlayerPawnBase::CreateFloorActor()
{

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.Instigator = this;

	FloorActor = GetWorld()->SpawnActor<AFloor>(FloorActorClass, FTransform(), SpawnParameters);
	FloorActor->AddActorWorldOffset(FVector::ZeroVector);
}

void APlayerPawnBase::DestroyAllAfterEnd()
{
	if (IsValid(FloorActor->FoodRef))
	{
		FloorActor->FoodRef->DestroiedTimeForEatCurrent = 15;
	}
	if (IsValid(FloorActor->FoodBonusFastSpeed))
	{
		FloorActor->FoodBonusFastSpeed->DestroiedTimeForEatCurrent = 15;
	}
	if (IsValid(FloorActor->FoodBonusSlowSpeed))
	{
		FloorActor->FoodBonusSlowSpeed->DestroiedTimeForEatCurrent = 15;
	}
	if (IsValid(FloorActor->FoodBonusSuperFastSpeed))
	{
		FloorActor->FoodBonusSuperFastSpeed->DestroiedTimeForEatCurrent = 15;
	}
	if (IsValid(FloorActor->FoodBonusSuperSlowSpeed))
	{
		FloorActor->FoodBonusSuperSlowSpeed->DestroiedTimeForEatCurrent = 15;
	}
}



//������ �������� SPACE FOR START AND UP-DOWN AND RIGHT-LEFT
void APlayerPawnBase::HandlePlayerStartGameInput(float value)
{
	if (value > 0)
	{
		SnakeActor->bIsGameStart = true;
		bIsGameStartExitMenu = true;
	}
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	//������ ������
	ASnakeElementBase* FirstSnakeElem = SnakeActor->SnakeElements[0];

	//������ ������ ������
	ASnakeElementBase* SecondSnakeElem = SnakeActor->SnakeElements[1];
	
	if (IsValid(SnakeActor) && bIsGameStartExitMenu)
	{
		//�������� � ������ ����������� ���� �� ���� ������ �� ������ ��������
		if ((value > 0) && 
			(SnakeActor->LastMoveDirection != EMovementDirection::DOWN) &&
			((FirstSnakeElem->GetActorLocation().Y != SecondSnakeElem->GetActorLocation().Y)))
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}

		//�������� � ������ ����������� ���� �� ���� ������ �� ������ ��������
		else if ((value < 0) && 
			(SnakeActor->LastMoveDirection != EMovementDirection::UP) &&
			((FirstSnakeElem->GetActorLocation().Y != SecondSnakeElem->GetActorLocation().Y)))
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	ASnakeElementBase* FirstSnakeElem = SnakeActor->SnakeElements[0];
	ASnakeElementBase* SecondSnakeElem = SnakeActor->SnakeElements[1];
	if (IsValid(SnakeActor) && bIsGameStartExitMenu)
	{
		//�������� � ������ ����������� ���� �� ���� ������ �� ������ ��������
		if ((value > 0) && 
			(SnakeActor->LastMoveDirection != EMovementDirection::RIGHT) &&
			((FirstSnakeElem->GetActorLocation().X != SecondSnakeElem->GetActorLocation().X)))
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		}

		//�������� � ������ ����������� ���� �� ���� ������ �� ������ ��������
		else if ((value < 0) && 
			(SnakeActor->LastMoveDirection != EMovementDirection::LEFT) &&
			((FirstSnakeElem->GetActorLocation().X != SecondSnakeElem->GetActorLocation().X)))
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
	}
}

void APlayerPawnBase::DecreaseTimeLevel()
{
	TimerForLevelCurrent = FMath::Max(TimerForLevelCurrent - 1, TimerForLevelMin);
}

//������ ��� ������ � Widgets
float APlayerPawnBase::ScoreGetForWidget()
{
	return SnakeActor->ScoreRightNow / SnakeActor->ScoreMax;
}

int APlayerPawnBase::HealthGetForWidget()
{
	return SnakeActor->Health;
}

float APlayerPawnBase::TimerGetForWidget()
{
	return TimerForLevelCurrent / TimerForLevelStart;
}
