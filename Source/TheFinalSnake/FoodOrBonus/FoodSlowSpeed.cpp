// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodSlowSpeed.h"
#include "TheFinalSnake/SnakeBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AFoodSlowSpeed::AFoodSlowSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodSlowSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodSlowSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsValid(this))
	{
		DestroiedTimeForEatCurrent += DeltaTime;

		if (DestroiedTimeForEatCurrent >= DestroiedTimeForEat)
		{
			AFoodSlowSpeed::Destroy();
		}
	}
}

void AFoodSlowSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		//����� �����
		if ((Snake->MovementSpeed + 0.01) <= (MinSpeed - 0.1f))
		{
			Snake->MovementSpeed += 0.01;
			Snake->SetActorTickInterval(Snake->MovementSpeed);
		}
		else
		{
			Snake->MovementSpeed = MinSpeed;
			Snake->SetActorTickInterval(Snake->MovementSpeed);
		}

		//����������� Actor ���
		AFoodSlowSpeed::Destroy();
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), EatSound, FVector(0, 0, 0));
	}
}

