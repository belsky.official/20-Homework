// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TheFinalSnake/Interactablee.h"
#include "Sound/SoundCue.h"
#include "Food.generated.h"

UCLASS()
class THEFINALSNAKE_API AFood : public AActor, public IInteractablee
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	//�������� �� ����������� ���, ���� � ����� �� ������ ������
	UPROPERTY()
	float   DestroiedTimeForEatCurrent = 0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float   DestroiedTimeForEat = 15.f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	USoundCue* EatSound;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//����� �������������� �� �������
	virtual void Interact(AActor* Interactor, bool bIsHead) override;

};
