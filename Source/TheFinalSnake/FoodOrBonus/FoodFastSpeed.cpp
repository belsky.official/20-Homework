// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodFastSpeed.h"
#include "TheFinalSnake/SnakeBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AFoodFastSpeed::AFoodFastSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodFastSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodFastSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsValid(this))
	{
		DestroiedTimeForEatCurrent += DeltaTime;

		if (DestroiedTimeForEatCurrent >= DestroiedTimeForEat)
		{
			AFoodFastSpeed::Destroy();
		}
	}
}

void AFoodFastSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			//����� �����
			if ((Snake->MovementSpeed - 0.01f) >= (MaxSpeed - 0.1f))
			{ 
				Snake->MovementSpeed -= 0.01f;
				Snake->SetActorTickInterval(Snake->MovementSpeed);
			}
			else
			{
				Snake->MovementSpeed = MaxSpeed;
				Snake->SetActorTickInterval(Snake->MovementSpeed);
			}

			//����������� Actor ���
			AFoodFastSpeed::Destroy();
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), EatSound, FVector(0, 0, 0));
		}
	}
}

