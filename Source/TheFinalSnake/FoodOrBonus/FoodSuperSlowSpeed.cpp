// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodSuperSlowSpeed.h"
#include "TheFinalSnake/SnakeBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AFoodSuperSlowSpeed::AFoodSuperSlowSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodSuperSlowSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodSuperSlowSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsValid(this))
	{
		DestroiedTimeForEatCurrent += DeltaTime;

		if (DestroiedTimeForEatCurrent >= DestroiedTimeForEat)
		{
			AFoodSuperSlowSpeed::Destroy();
		}
	}

}

void AFoodSuperSlowSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		//����� �����
		if ((Snake->MovementSpeed + 0.01f) >= MinSpeed)
		{
			Snake->MovementSpeed = MinSpeed;
			Snake->SetActorTickInterval(Snake->MovementSpeed);
		}
		AFoodSuperSlowSpeed::Destroy();
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), EatSound, FVector(0, 0, 0));

	}
}

