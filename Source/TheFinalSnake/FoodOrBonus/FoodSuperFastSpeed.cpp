// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodSuperFastSpeed.h"
#include "TheFinalSnake/SnakeBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AFoodSuperFastSpeed::AFoodSuperFastSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodSuperFastSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodSuperFastSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsValid(this))
	{
		DestroiedTimeForEatCurrent += DeltaTime;

		if (DestroiedTimeForEatCurrent >= DestroiedTimeForEat)
		{
			AFoodSuperFastSpeed::Destroy();
		}
	}
}

void AFoodSuperFastSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		//����� �����
		if ((Snake->MovementSpeed - 0.01f) >= MaxSpeed)
		{
			Snake->MovementSpeed = MaxSpeed;
			Snake->SetActorTickInterval(Snake->MovementSpeed);
		}

		//����������� Actor ���
		AFoodSuperFastSpeed::Destroy();
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), EatSound, FVector(0, 0, 0));

	}
}

