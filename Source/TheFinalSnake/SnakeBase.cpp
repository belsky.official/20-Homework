// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "MagicNumberTypes.h"
#include "PlayerPawnBase.h"
#include "Kismet/GameplayStatics.h"
#include "Interactablee.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tck() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = ELEMENT_SIZE;
	MovementSpeed = 0.15f;
	LastMoveDirection = EMovementDirection::DOWN;
	bIsFirstSpawn = true;
	ScoreRightNow = 0.0f;
	ScoreMax = 175.0f;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();

	//��������� ������������ ��������� ����� ����� ��� �������� ������
	SetActorTickInterval(MovementSpeed);

	//�������� ������ � ������ ���� �������� - 5
	AddSnakeElement(3);
	//PlayerPawnBase = Cast<APlayerPawnBase>(GetInstigatorController()->GetPawn());
}


// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (bIsGameStart)
	{
		Move();
	}
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation;
		if (bIsFirstSpawn)
		{
			NewLocation.Set(505, 0, 0);
			bIsFirstSpawn = false;	
		}
		else
		{
			NewLocation = SnakeElements.Last()->GetActorLocation();
			NewLocation.X += ElementSize;
		}
		//����������� ��������������� ��� ������ ������ �������� ������
		FTransform NewTransform(NewLocation);

		//����� ������ �������� ������
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		
		//��������� ������ �������� ��� Actor SnakeOwner ��� ������ � SnakeElement.h
		NewSnakeElem->SnakeOwner = this;
		//����������� ������ ���������� ������������ �������� ��� ����������� ������ ������
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}	
		if (ElemIndex != 0 && ElemIndex % 2 == 0)
		{
			SnakeElements[ElemIndex]->MeshComponent->SetMaterial(0, ElementMaterialFirst);

		}
		if (ElemIndex != 0 && ElemIndex % 2 != 0)
		{
			SnakeElements[ElemIndex]->MeshComponent->SetMaterial(0, ElementMaterialSecond);
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(FVector::ZeroVector);
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}
	
	//���������� �������� ��� ������
	SnakeElements[0]->ToggleCollision();

	//��������� ��������� ���� ���������, ����� ������
	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	//������� ��������� ������ ������
	SnakeElements[0]->AddActorWorldOffset(MovementVector);

	//��������� �������� ����� ���������� � 83 �������
	SnakeElements[0]->ToggleCollision();
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), MoveSound, FVector(0, 0, 0));
}

//����� ��������������� ������ ������ � ������ Actor
void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		//����� ������ ������
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractablee* IInteractableInterface = Cast<IInteractablee>(Other);
		if (IInteractableInterface)
		{
			IInteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::Destroyed()
{
	Super::Destroyed();

	for (int i = 1; i < SnakeElements.Num(); i++)
	{
		SnakeElements[i]->MeshComponent->SetMaterial(0, DeadMaterial);
	}
	//PlayerPawnBase->PlaySound();
	
}