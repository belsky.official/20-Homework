// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TheFinalSnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class THEFINALSNAKE_API ATheFinalSnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
