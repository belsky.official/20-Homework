// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Sound/SoundCue.h"
#include "PlayerPawnBase.generated.h"

//������� ����������� ��� ����������� ������ � cpp �����
class UCameraComponent; 
class ASnakeBase;
class AFloor;
class AFood;
class AFoodFastSpeed;
class AFoodSlowSpeed;
class AFoodSuperFastSpeed;
class AFoodSuperSlowSpeed;

UCLASS()
class THEFINALSNAKE_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	//�������� ������ � BP ��� ������ � ������ ���������� PawnCamera
	UPROPERTY(BlueprintReadWrite) 
	UCameraComponent* PawnCamera;

	//�������� ������ � BP ��� ������ � ������ Actor ������
	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;

	UPROPERTY(BlueprintReadWrite)
	AFloor* FloorActor;

	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	USoundCue* MoveSound;

	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	USoundCue* EatSound;

	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	USoundCue* DiedSound;

	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	USoundCue* PortalSound;

	//� ���������� ��������� ��������� ��� ������ ������ ������
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFloor> FloorActorClass;

	

	//���������� ��� ������� ������
	FTimerHandle DecreaseTimerHandle;
	UPROPERTY(BlueprintReadOnly)
	float TimerForLevelMin = 0; float TimerForLevelCurrent;

	UPROPERTY(EditDefaultsOnly)
	float TimerForLevelStart = 600.0f;

	//����������� ��� ������ ���� (������ Space)
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool bIsGameStartExitMenu = false;

	//��� ������ ��� ����� ������� ����������� ������ ������ ��� ��������
	UPROPERTY(BlueprintReadWrite)
	bool bIsOpenWidgetMenuRestartExit = false;

	//��������� �������� ������ ��� � �������

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Timer of Bonus")
	int HowOftenSpawnBonus = 4;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Timer of Bonus")
	int HowOftenSpawnSuperBonus = 11;

	int BufferTimerForBonus = 0;
	int BufferTimerForSuperBonus = 0;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//����� ������ ������ ������� ����
	void CreateSnakeActor();

	//����� ������ ���� ������� ����
	void CreateFloorActor();
	
	void DestroyAllAfterEnd();

	UFUNCTION()
	void HandlePlayerStartGameInput(float value);

	//������� � �������� � BP �� ��������� ������ �������� UP-DOWN
	UFUNCTION()
	void HandlePlayerVerticalInput(float value);

	//������� � �������� � BP �� ��������� ������ �������� RIGHT-LEFT
	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

	//����� ���������� ������� �� ����� ������
	void DecreaseTimeLevel();


	/*	������ ��� Widgets:	*/	

	//��������� � ���������� ����������� ���������� Score
	UFUNCTION(BlueprintCallable)
	float ScoreGetForWidget();

	//��������� ���������� ���������� ������ � ������
	UFUNCTION(BlueprintCallable)
	int HealthGetForWidget();

	//��������� ���������� ����� �� ����� ������
	UFUNCTION(BlueprintCallable)
	float TimerGetForWidget();
};
